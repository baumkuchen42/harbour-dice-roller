import QtQuick 2.6
import Sailfish.Silica 1.0

 Dialog {
     property string eyes

     Column {
         width: parent.width

         DialogHeader { title: "Manual input for dice" }

         TextField {
             id: numberField
             inputMethodHints: Qt.ImhFormattedNumbersOnly
             label: "Dice eyes"
             focus: true
         }
     }

     onDone: {
         if (result == DialogResult.Accepted) {
             eyes = numberField.text
         }
     }
 }
