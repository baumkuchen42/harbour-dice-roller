import QtQuick 2.6
import Sailfish.Silica 1.0

import "../js/functions.js" as Functions

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

//        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
//        PullDownMenu {
//            MenuItem {
//                text: qsTr("Settings")
//                onClicked: pageStack.animatorPush(Qt.resolvedUrl("SecondPage.qml"))
//            }
//        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Dice Roller")
            }
            SectionHeader {
                text: "First dice"
            }
            Row {
                width: page.width * 2/3
                spacing: Theme.paddingSmall
                anchors.horizontalCenter: parent.horizontalCenter
                ListModel {
                    id: diceModel
                    ListElement {
                        name: "D4"
                        value: 4
                    }
                    ListElement {
                        name: "D6"
                        value: 6
                    }
                    ListElement {
                        name: "D8"
                        value: 8
                    }
                    ListElement {
                        name: "D10"
                        value: 10
                    }
                    ListElement {
                        name: "D12"
                        value: 12
                    }
                    ListElement {
                        name: "D20"
                        value: 20
                    }
                }
                ComboBox {
                    id: diceChooser
                    label: "Dice"
                    width: page.width / 3
                    menu: ContextMenu {
                        Repeater {
                            model: diceModel
                            MenuItem { text: model.name }
                        }
                        MenuItem {
                            text: "Custom"
                            onClicked: {
                                var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/DiceInputDialog.qml"), {})
                                dialog.accepted.connect(function() {
                                    diceModel.append({
                                        "name": "D"+dialog.eyes.toString(),
                                        "value": parseInt(dialog.eyes, 10)
                                    })
                                    diceChooser.currentIndex = diceModel.count - 1
                                })
                            }
                        }
                    }
                }
                TextField {
                    id: diceCountField
                    width: page.width / 3
                    text: "1"
                    inputMethodHints: Qt.ImhFormattedNumbersOnly
                    label: "Count"
                }
            }
            // TODO make Add dice button of roll button and move roll function to drop down menu
            ButtonLayout {
                Button {
                    text: "Roll"
                    onClicked: {
                        var dice = Functions.getDiceValueByName(diceChooser.value)
                        var count = parseInt(diceCountField.text, 10)
                        var result = Functions.roll(dice, count)

                        resultText.text = result.toString()
                        resultSectionHeader.visible = true
                        resultText.visible = true
                    }
                }
            }
            SectionHeader {
                id: resultSectionHeader
                text: "Result"
                visible: false
            }
            Label {
                id: resultText
                text: "Result will appear here"
                color: palette.highlightColor
                font.pixelSize: Theme.fontSizeLarge
                anchors.horizontalCenter: parent.horizontalCenter
                visible: false
            }
        }
    }
}
